# based on http://people.csail.mit.edu/hubert/pyaudio/
# """PyAudio example: Record a few seconds of audio and save to a WAVE file."""

import numpy
import pyaudio
from pylsl import *
import argparse
import sys

CHUNK_SIZE = 160  # both audeering and zamia process only 160 frame-chunks
FORMAT = pyaudio.paFloat32
FORMAT_SIZE = 4  # how much bytes are needed per sample; needs to match the format specified above
CHANNELS = 1
RATE = 16000

parser = argparse.ArgumentParser(description='Sends microphone samples to LSL.')
parser.add_argument('--device-index', '-di', dest='device_index', type=int, default=None,
                    help='Specifies the used microphone device. '
                         'If nothing is specified the system\'s default microphone device will be used.')
args = parser.parse_args()
device_index = args.device_index


p = pyaudio.PyAudio()


#https://stackoverflow.com/questions/36894315/how-to-select-a-specific-input-device-with-pyaudio#39677871
print("Available devices:")
info = p.get_host_api_info_by_index(0)
numdevices = info.get('deviceCount')
for i in range(0, numdevices):
        if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            print("\tInput Device index " + str(i) + " - " + p.get_device_info_by_host_api_device_index(0, i).get('name'))
            numdevices = i + 1 # info.get('deviceCount') seems to return not only audio devices -> set the correct value


device_info = None
if device_index is None:
    device_info = p.get_default_input_device_info()
    print("Using the systems default microphone device: index" + str(device_info['index']))
else:
    if device_index >= numdevices or device_index < 0:
        print('Argument --device-index is out of bounds.')
        sys.exit()

    device_info = p.get_device_info_by_host_api_device_index(0, device_index)
    print("Using audio device " + str(device_index))


print("Advanced device info:")
for key, value in device_info.items():
    print("\t" + str(key) + "\t" + str(value))


stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                input_device_index=device_index,
                frames_per_buffer=CHUNK_SIZE)


outlet_info = StreamInfo('AudioCapture', 'Audio', 1, RATE, cf_float32, 'uid209483735')
outlet_metadata_device = outlet_info.desc().append_child('RecordingDevice')
for key, value in device_info.items():
    outlet_metadata_device.append_child_value(str(key), str(value))

outlet = StreamOutlet(outlet_info)

print("* recording")

while True:
    # it seems (although I found no information about it in the documentation) that audioop.getsample() only returns
    # int values. Dividing them by 65536 is ugly but as using audioop is the "official" way of retrieving samples I will
    # keep it in
    #
    # data = stream.read(CHUNK_SIZE)
    # samples = [ ]
    # for i in range(0, CHUNK_SIZE):
    # samples.append(audioop.getsample(data, FORMAT_SIZE, i) / 65536.0)

    # taken from https://stackoverflow.com/questions/19629496/get-an-audio-sample-as-float-number-from-pyaudio-stream
    data = stream.read(CHUNK_SIZE * FORMAT_SIZE)
    decoded = numpy.fromstring(data, 'Float32')
    outlet.push_chunk(decoded)

# theoretically theses function would have to be invoked to properly end the recording
# stream.stop_stream()
# stream.close()
# p.terminate()

# if you want to stop the recording before the script ends don't forget to also terminate the LSL outlet
